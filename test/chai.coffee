mongoose = require 'mongoose'
chai = require 'chai'
spies = require 'sinon-chai'

chai.use(spies)

chai.config.includeStack = true
global.expect = chai.expect
global.AssertionError = chai.AssertionError
global.Assertion = chai.Assertion
global.assert = chai.assert
global.sinon = require 'sinon'



