shortener = require '../lib/shortener'
ObjectId = require('bson').ObjectId

describe 'shortener', ->
  it 'generates a short id', ->
    hash = shortener(new ObjectId)
    expect(hash.length).to.be < 8
    expect(hash.length).to.be > 2

  it 'generates the same hash', ->
    id = new ObjectId
    hash1 = shortener(id)
    hash2 = shortener(id)
    expect(hash2).to.be.equal hash2

  it 'generates different hash', ->
    hash1 = shortener(new ObjectId)
    hash2 = shortener(new ObjectId)

    expect(hash1).to.not.equal hash2

  it 'doesnt duplicate many hashes', ->
    total = 1000
    current = 0
    lookup = {}
    while current < total
      current++
      id = new ObjectId
      hash = shortener(id)
      expect(lookup[hash]).to.not.be.true
      lookup[hash] = true




