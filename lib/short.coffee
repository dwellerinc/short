###
# @list dependencies
###

generateHash = require './shortener'
mongoose = require('mongoose')
Promise = require('node-promise').Promise
ShortURL = require('../models/ShortURL').ShortURL
Metrics = require('../models/ShortURL').Metrics
HtmlAttribute = require('../models/ShortURL').HtmlAttribute
HtmlElement = require('../models/ShortURL').HtmlElement
exports.ShortURL = ShortURL
exports.Metrics = Metrics
exports.HtmlElement = HtmlElement
exports.HtmlAttribute = HtmlAttribute

###*
# @configure short-id
###


###*
# @method connect
# @param {String} mongdb Mongo DB String to connect to
###

exports.connect = (mongodb) ->
  if mongoose.connection.readyState == 0
    mongoose.connect mongodb
  mongoose.Promise = require('bluebird')
  exports.connection = mongoose.connection
  return

###*
# @method generate
# @param {Object} options Must at least include a `URL` attribute
###

exports.generate = (document) ->
  generatePromise = undefined
  promise = new Promise
  ShortURL.findOne({
    URL: document.URL
    user: document.user
  }).then (existing) ->
    if existing
      return existing

    totalMetrics = new Metrics(
      clicks: 0
      impressions: 0
      destinationClicks: 0)
    totalMetrics.save (err) ->
      if err
        console.log err
        return
      document['totalMetrics'] = totalMetrics._id

      generatePromise = ShortURL.create({
        user: document.user,
        URL: document.URL,
        totalMetrics: totalMetrics
      }).then((shortUrl) ->
        shortUrl.hash = generateHash(shortUrl._id)
        shortUrl.save()
      )

      generatePromise.then ((ShortURLObject) ->
        promise.resolve ShortURLObject
        return
      ), (error) ->
        console.log error
        promise.reject error, true
        return
      return
    promise

###*
# @method retrieve
# @param {Object} options Must at least include a `hash` attribute
###

exports.retrieve = (hash) ->
  promise = new Promise
  query = hash: hash
  retrievePromise = ShortURL.findOne(query).populate('totalMetrics').populate('dailyMetrics').populate('hourlyMetrics').exec()
  retrievePromise.then ((ShortURLObject) ->
    if ShortURLObject and ShortURLObject != null
      promise.resolve ShortURLObject
    else
      promise.reject new Error('MongoDB - Cannot find Document'), true
    return
  ), (error) ->
    console.log error
    promise.reject error, true
    return
  promise

###*
# @method update
# @param {String} hash - must include a `hash` attribute
# @param {Object} updates - must include either a `URL` or `data` attribute
###

exports.update = (hash, updates) ->
  promise = new Promise
  ShortURL.findOne { hash: hash }, (err, doc) ->
    if updates.URL
      doc.URL = updates.URL
    if updates.data
      doc.data = extend(doc.data, updates.data)
      doc.markModified 'data'
      #Required by mongoose, as data is of Mixed type
    doc.save (err, updatedObj, numAffected) ->
      if err
        promise.reject new Error('MongoDB - Cannot save updates'), true
      else
        promise.resolve updatedObj
      return
    return
  promise

###*
# @method hits
# @param {Object} options Must at least include a `hash` attribute
###

exports.hits = (hash) ->
  promise = new Promise
  query = hash: hash
  options = multi: true
  retrievePromise = ShortURL.findOne(query)
  retrievePromise.then ((ShortURLObject) ->
    if ShortURLObject and ShortURLObject != null
      promise.resolve ShortURLObject.hits
    else
      promise.reject new Error('MongoDB - Cannot find Document'), true
    return
  ), (error) ->
    promise.reject error, true
    return
  promise

###*
# @method list
# @description List all Shortened URLs
###

exports.list = ->
  ShortURL.find {}

###*
# @method extend
# @description Private function to extend objects
# @param {Object} original The original object to extend
# @param {Object} updated The updates; new keys are added, existing updated
###

extend = (original, updates) ->
  Object.keys(updates).forEach (key) ->
    original[key] = updates[key]
    return
